"""Main bookkeeping webapp."""
from flask import Flask, redirect, url_for

from .auth import setup_oauth, require_login, auth_login, auth_authorize, auth_logout
from .config import APPLICATION_ROOT, SECRET

from .webpage import WebpageResponse
from .webpage.index import page_index
from .webpage.plots import page_plots, page_plots_list, send_plot
from .webpage.samples import page_samples, page_samples_list
from .webpage.tasks import (
    page_tasks,
    page_tasks_list,
    page_tasks_download,
    page_tasks_download_metadata,
    page_tasks_replicate,
)

application = Flask(
    __name__,
    static_url_path=(APPLICATION_ROOT + 'assets'),
    template_folder='../templates',
    static_folder='../public/assets',
)

application.config.from_object('analysis_bookkeeping.config')
application.secret_key = SECRET

oauth = setup_oauth(application)


@application.route(APPLICATION_ROOT + 'login')
def login() -> WebpageResponse:
    """Login."""
    return auth_login(oauth)


@application.route(APPLICATION_ROOT + 'auth')
def auth() -> WebpageResponse:
    """Authorize."""
    return auth_authorize(oauth)


@application.route(APPLICATION_ROOT + 'logout')
def logout() -> WebpageResponse:
    """Logout."""
    return auth_logout()


@application.route(APPLICATION_ROOT)
def index() -> WebpageResponse:
    """Home page."""
    return page_index()


@application.route(APPLICATION_ROOT + '<in_page>')
def default_redirect(in_page: str) -> WebpageResponse:
    """Redirect to home page by default."""
    return redirect(url_for('index'))


@application.route(APPLICATION_ROOT + 'plots/view/<path:filename>')
@require_login
def plots_view(filename: str) -> WebpageResponse:
    """View plots."""
    return send_plot(filename)


@application.route(APPLICATION_ROOT + 'plots/list/<in_group>/<in_revision>/<in_config>')
@require_login
def plots_list(in_group: str, in_revision: str, in_config: str) -> WebpageResponse:
    """List plots."""
    return page_plots_list(in_group, in_revision, in_config)


@application.route(APPLICATION_ROOT + 'plots')
@require_login
def plots() -> WebpageResponse:
    """Load plots."""
    return page_plots()


@application.route(APPLICATION_ROOT + 'samples/<in_group>')
@require_login
def samples_list(in_group: str) -> WebpageResponse:
    """Load samples list."""
    return page_samples_list(in_group)


@application.route(APPLICATION_ROOT + 'samples')
@require_login
def samples() -> WebpageResponse:
    """Load samples."""
    return page_samples()


@application.route(APPLICATION_ROOT + 'tasks')
@require_login
def tasks() -> WebpageResponse:
    """Load tasks."""
    return page_tasks()


@application.route(APPLICATION_ROOT + 'tasks/<in_campaign>')
def tasks_list_redirect(in_campaign: str) -> WebpageResponse:
    """Load tasks for a campaign."""
    return redirect(url_for('tasks'))


@application.route(APPLICATION_ROOT + 'tasks/<in_campaign>/<in_group>')
@require_login
def tasks_list(in_campaign: str, in_group: str) -> WebpageResponse:
    """Load tasks for a campaign and a group."""
    return page_tasks_list(in_campaign, in_group)


@application.route(APPLICATION_ROOT + 'tasks/<in_campaign>/<in_group>/download')
@require_login
def tasks_download(in_campaign: str, in_group: str) -> WebpageResponse:
    """Download files for a campaign and a group."""
    return page_tasks_download(in_campaign, in_group)


@application.route(
    APPLICATION_ROOT + 'tasks/<in_campaign>/<in_group>/download_metadata'
)
@require_login
def tasks_download_metadata(in_campaign: str, in_group: str) -> WebpageResponse:
    """Download metadata for a campaign and a group."""
    return page_tasks_download_metadata(in_campaign, in_group)


@application.route(APPLICATION_ROOT + 'tasks/<in_campaign>/<in_group>/replicate')
def tasks_replicate_redirect(in_campaign: str, in_group: str) -> WebpageResponse:
    """Replicate datasets for a campaign and a group."""
    return redirect(
        url_for(  # type: ignore
            'tasks_replicate',
            {'in_campaign': in_campaign, 'in_group': in_group, 'in_rse': 'RSE'},
        )
    )


@application.route(
    APPLICATION_ROOT + 'tasks/<in_campaign>/<in_group>/replicate/<in_rse>'
)
@require_login
def tasks_replicate(in_campaign: str, in_group: str, in_rse: str) -> WebpageResponse:
    """Replicate datasets for a campaign and a group."""
    return page_tasks_replicate(in_campaign, in_group, in_rse)
