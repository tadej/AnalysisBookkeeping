"""Authentication support."""
from authlib.integrations.flask_client import OAuth
from authlib.oauth2.auth import OAuth2Token
from flask import Flask, redirect, request, session, url_for
from functools import wraps
from typing import Any, Callable

from .config import LOGIN_REQUIRED
from .webpage import WebpageResponse


def setup_oauth(application: Flask) -> OAuth:
    """Configure oauth support."""
    oauth = OAuth(
        application,
    )
    oauth.register(
        name='CERN',
        server_metadata_url='https://auth.cern.ch/auth/realms/cern/.well-known/openid-configuration',
        client_kwargs={'scope': 'openid profile'},
    )
    return oauth


def require_login(f: Callable[..., WebpageResponse]) -> Callable[..., WebpageResponse]:
    """Require login helper."""

    @wraps(f)
    def decorated_function(*args: Any, **kwargs: Any) -> WebpageResponse:
        if LOGIN_REQUIRED and 'user' not in session:
            return redirect(url_for('login', next_url=request.url))
        return f(*args, **kwargs)

    return decorated_function


def auth_login(oauth: OAuth) -> WebpageResponse:
    """Login."""
    if 'next_url' in request.args:
        session['next_url'] = request.args['next_url']
    else:
        session['next_url'] = url_for('index')

    redirect_uri = url_for('auth', _external=True)
    return oauth.CERN.authorize_redirect(redirect_uri)


def auth_authorize(oauth: OAuth) -> WebpageResponse:
    """Authorize."""
    token = oauth.CERN.authorize_access_token()
    user = oauth.CERN.parse_id_token(token)
    session['user'] = user

    url = session['next_url']
    session.pop('next_url', None)

    # do something with the token and profile
    if url:
        return redirect(url)
    return redirect(url_for('index'))


def auth_logout() -> WebpageResponse:
    """Logout."""
    session.pop('user', None)
    return redirect(url_for('index'))
