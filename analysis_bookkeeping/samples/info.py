"""Samples info helpers."""
from copy import deepcopy
from json import load
from pathlib import Path
from typing import Dict, List, Set, Tuple

from ..config import APPLICATION_CONFIG
from .types import MCCampaign, Sample, SamplesList


def get_samples(file: Path) -> SamplesList:
    """List samples from group."""
    with file.open('r') as f:
        data = load(f)

    samples: SamplesList = []
    for d in data:
        samples.append(Sample.from_dict(d))
    return samples


def get_samples_campaigns(samples: SamplesList) -> List[MCCampaign]:
    """List MC campaigns for sample type."""
    campaigns = set()
    for sample in samples:
        for c in sample.campaigns:
            campaigns.add(c)

    campaigns_list = list(campaigns)
    campaigns_list.sort(key=lambda x: x.value)
    return campaigns_list


def get_samples_groups(sample: str, groups_summary: Dict[str, List[str]]) -> Set[str]:
    """Get samples groups."""
    sample = sample.replace('_signal', '')
    sample = sample.replace('__', '+')

    groups = set()
    for g, samples in groups_summary.items():
        for s in samples:
            if s == sample:
                groups.add(g)
                break

    return groups


def get_samples_derivations(samples: SamplesList, derivation: str) -> List[str]:
    """List samples derivations from list."""
    dsids = []
    for sample in samples:
        for daod in sample.datasets.DAOD:
            if daod.derivation == derivation:
                dsids.append(daod.dataset)
    return dsids


def get_derivations_types(samples: SamplesList) -> List[str]:
    """List derivation types for sample type."""
    types = set()
    for sample in samples:
        for daod in sample.datasets.DAOD:
            types.add(daod.derivation)

    types_list = list(types)
    types_list.sort()
    return types_list


def check_derivations(
    samples: SamplesList,
) -> Tuple[Dict[Sample, List[str]], Dict[Sample, List[str]]]:
    """Check derivations in samples."""
    if 'required_derivations' not in APPLICATION_CONFIG:
        required = set()
    else:
        required = set(APPLICATION_CONFIG['required_derivations'])

    result_available = {}
    result_missing = {}
    for sample in samples:
        available = set()
        missing = deepcopy(required)

        for daod in sample.datasets.DAOD:
            available.add(daod.derivation)

            try:
                missing.remove(daod.derivation)
            except KeyError:
                pass

        result_available[sample] = list(available)
        result_missing[sample] = list(missing)
    return result_available, result_missing
