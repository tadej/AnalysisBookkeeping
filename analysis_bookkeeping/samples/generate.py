"""Sample info generation helpers."""
import re
from typing import Dict, List

from ..config import SAMPLE_DATABASE_PATH

from .types import MCCampaign, SampleDerivation, SamplesInfo, SampleMetadata


def generate_xsec_info(samples: SamplesInfo, filename: str) -> None:
    """Read and generate list of x-sec info."""
    path = SAMPLE_DATABASE_PATH / 'Info'

    with open(path / filename, 'r') as f:
        for line in f:
            line_split = line.split()
            if line_split[0] == 'DSID':
                continue

            dsid = int(line_split[0])

            if not samples[dsid].metadata:
                samples[dsid].metadata = SampleMetadata()

            metadata = samples[dsid].metadata
            if not metadata:
                continue

            metadata.AMI_xsec = float(line_split[2])
            metadata.filter_efficiency = float(line_split[3])
            metadata.k_factor = float(line_split[4])
            metadata.total_xsec = float(line_split[5])
            metadata.xsec_uncert = float(line_split[6])

            samples[dsid].name = line_split[1]
    pass


def generate_aod_list(samples: SamplesInfo, filename: str) -> None:
    """Read and generate list of AODs."""
    path = SAMPLE_DATABASE_PATH / 'AOD'

    with open(path / filename, 'r') as f:
        for line in f:
            item = line.split('#')[0].strip()
            if not item:
                continue

            search_dsid = re.search('mc16_13TeV.([0-9]+)', item)
            search_stag = re.search('_([as][0-9]+)', item)
            search_rtag = re.search('_(r[0-9]+)', item)
            if not search_dsid or not search_stag or not search_rtag:
                raise ValueError(f'Invalid AOD: {item}')

            dsid = int(search_dsid.group(1))
            if dsid not in samples:
                continue

            stag = search_stag.group(1)
            rtag = search_rtag.group(1)

            sample = samples[dsid]
            if 'a' in stag:
                sample.AFII = True

            sample.campaigns.append(MCCampaign.from_tag(rtag))
            sample.datasets.AOD.append(item)
    pass


def generate_pileup_list(samples: SamplesInfo, filename: str) -> None:
    """.Read and generate list of NTUP_PILEUP files."""
    path = SAMPLE_DATABASE_PATH / 'NTUP_PILEUP'

    files = [f for f in path.iterdir() if f.is_file() and filename in f.name]

    for file in files:
        with file.open('r') as f:
            for line in f:
                item = line.split('#')[0].strip()
                if not item:
                    continue

                search_dsid = re.search('mc16_13TeV.([0-9]+)', item)
                if not search_dsid:
                    raise ValueError(f'Invalid NTUP_PILEUP: {item}')

                dsid = int(search_dsid.group(1))
                if dsid not in samples:
                    continue

                sample = samples[dsid]
                sample.datasets.NTUP_PILEUP.append(item)
    pass


def generate_daod_list(samples: SamplesInfo, filename: str) -> None:
    """Read and generate list of DAODs."""
    keyword = filename[:-4].replace('_signal', '')
    path = SAMPLE_DATABASE_PATH / 'DAOD'

    folders = [d for d in path.iterdir() if d.is_dir()]

    for d in folders:
        files = [
            f
            for f in d.iterdir()
            if f.is_file() and keyword in f.name and 'extra' not in f.name
        ]

        for file in files:
            with file.open('r') as f:
                for line in f:
                    item = line.split('#')[0].strip()
                    if not item:
                        continue

                    search_dsid = re.search('mc16_13TeV.([0-9]+)', item)
                    search_derivation = re.search('DAOD_([A-Z]+[0-9]?[0-9]?)', item)
                    search_ptag = re.search('_(p[0-9]+)', item)
                    if not search_dsid or not search_derivation or not search_ptag:
                        raise ValueError(f'Invalid DAOD: {item}')

                    dsid = int(search_dsid.group(1))
                    if dsid not in samples:
                        continue

                    derivation = search_derivation.group(1)
                    ptag = search_ptag.group(1)

                    sample = samples[dsid]
                    sample.datasets.DAOD.append(
                        SampleDerivation(item, derivation, ptag)
                    )
    pass


def generate_daod_list_data(samples: SamplesInfo, keyword: str = 'Data') -> None:
    """Read and generate list of DAODs for data."""
    path = SAMPLE_DATABASE_PATH / 'DAOD'

    folders = [d for d in path.iterdir() if d.is_dir()]

    for d in folders:
        files = [
            f
            for f in d.iterdir()
            if f.is_file()
            and keyword in f.name
            and 'extra' not in f.name
            and 'debug' not in f.name
        ]

        for file in files:
            with file.open('r') as f:
                for line in f:
                    item = line.split('#')[0].strip()
                    if not item:
                        continue

                    search_dsid = re.search(
                        'data[0-9][0-9]_13TeV.period([A-Za-z]+)', item
                    )
                    search_derivation = re.search('DAOD_([A-Z]+[0-9]?[0-9]?)', item)
                    search_ptag = re.search('_(p[0-9]+)', item)
                    if not search_dsid or not search_derivation or not search_ptag:
                        raise ValueError(f'Invalid DAOD: {item}')

                    dsid = search_dsid.group(0)
                    if dsid not in samples:
                        continue

                    derivation = search_derivation.group(1)
                    ptag = search_ptag.group(1)

                    sample = samples[dsid]
                    sample.datasets.DAOD.append(
                        SampleDerivation(item, derivation, ptag)
                    )
    pass


def generate_groups_summary() -> Dict[str, List[str]]:
    """Read and generate sample groups."""
    path = SAMPLE_DATABASE_PATH / 'Groups'

    files = [f for f in path.iterdir() if f.is_file()]

    summary = {}

    for file in files:
        with file.open('r') as f:
            outname = file.stem
            outlist = []

            for line in f:
                item = line.split('#')[0].strip()
                if not item:
                    continue

                outlist.append(item)

            summary[outname] = outlist

    return summary
