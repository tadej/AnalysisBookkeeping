"""Sample information types."""
from enum import Enum
from pathlib import Path
from typing import Any, Dict, List, Optional, Union


class MCCampaign(Enum):
    """MC campaign."""

    MC16a = 'MC16a'
    MC16d = 'MC16d'
    MC16e = 'MC16e'

    @classmethod
    def from_string(cls, string: str) -> 'MCCampaign':
        """Get MC campaign from r-tag."""
        if string == 'MC16a':
            return MCCampaign.MC16a
        if string == 'MC16d':
            return MCCampaign.MC16d
        if string == 'MC16e':
            return MCCampaign.MC16e
        raise ValueError('Invalid string')

    @classmethod
    def from_tag(cls, tag: str) -> 'MCCampaign':
        """Get MC campaign from r-tag."""
        if tag == 'r9364':
            return MCCampaign.MC16a
        if tag == 'r10201':
            return MCCampaign.MC16d
        if tag == 'r10724':
            return MCCampaign.MC16e
        raise ValueError('Invalid r-tag')


class SampleMetadata:
    """Sample metadata class."""

    def __init__(self) -> None:
        """Initialize sample metadata."""
        self.total_xsec: float = -1.0
        self.xsec_uncert: float = 0.0
        self.AMI_xsec: float = -1.0
        self.filter_efficiency: float = -1.0
        self.k_factor: float = -1.0

    @classmethod
    def from_dict(cls, dict: Dict[str, float]) -> 'SampleMetadata':
        """Initialize sample metadata from a dictionary."""
        sample = cls()
        sample.total_xsec = dict['total_xsec']
        sample.xsec_uncert = dict['xsec_uncert']
        sample.AMI_xsec = dict['AMI_xsec']
        sample.filter_efficiency = dict['filter_efficiency']
        sample.k_factor = dict['k_factor']
        return sample


class SampleDerivation:
    """Sample derivation class."""

    def __init__(self, dataset: str, derivation: str, tag: str) -> None:
        """Initialize sample derivation."""
        self.dataset: str = dataset
        self.derivation: str = derivation
        self.tag: str = tag


class SampleDatasets:
    """Sample datasets class."""

    def __init__(
        self,
        aod: Optional[List[str]] = None,
        pileup: Optional[List[str]] = None,
        daod: Optional[List[Dict[str, Any]]] = None,
    ) -> None:
        """Initialize sample datasets."""
        self.AOD: List[str] = aod if aod else []
        self.NTUP_PILEUP: List[str] = pileup if pileup else []
        self.DAOD: List[SampleDerivation] = []

        if daod:
            for d in daod:
                self.DAOD.append(
                    SampleDerivation(d['dataset'], d['derivation'], d['tag'])
                )


SampleDSID = Union[int, str]


class Sample:
    """Sample info class."""

    def __init__(self, dsid: SampleDSID, label: str) -> None:
        """Initialize sample."""
        self.DSID: SampleDSID = dsid
        self.label: str = label
        self.name: str = ''
        self.MC: bool = False
        self.AFII: bool = False
        self.year: Optional[int] = None
        self.period: Optional[str] = None
        self.campaigns: List[MCCampaign] = []
        self.metadata: Optional[SampleMetadata] = None
        self.datasets = SampleDatasets()

    @classmethod
    def from_dict(cls, dict: Dict[str, Any]) -> 'Sample':
        """Initialize sample from a dictionary."""
        sample = cls(dict['DSID'], dict['label'])
        sample.name = dict['name']
        sample.MC = dict['MC']
        sample.AFII = dict['AFII']
        sample.year = dict['year']
        sample.period = dict['period']
        sample.campaigns = [MCCampaign.from_string(c) for c in dict['campaigns']]
        sample.metadata = (
            SampleMetadata.from_dict(dict['metadata'])
            if 'metadata' in dict and dict['metadata']
            else None
        )
        sample.datasets = SampleDatasets(
            dict['datasets']['AOD'],
            dict['datasets']['NTUP_PILEUP'],
            dict['datasets']['DAOD'],
        )
        return sample


SamplesInfo = Dict[SampleDSID, Sample]
SamplesList = List[Sample]


class SampleGroupBase:
    """Sample group base class."""

    def __init__(self, title: str, url: str, file: Path) -> None:
        """Initialize sample group base."""
        self.title: str = title
        self.url: str = url
        self.file: Path = file
        self.errors: bool = False

    @classmethod
    def from_dict(cls, dict: Dict[str, Any]) -> 'SampleGroupBase':
        """Initialize sample group base from a dictionary."""
        group = cls(dict['title'], dict['url'], Path(dict['file']))
        group.errors = dict['errors']
        return group


class SampleGroup(SampleGroupBase):
    """Sample group class."""

    def __init__(self, title: str, url: str, file: Path) -> None:
        """Initialize sample group."""
        super(SampleGroup, self).__init__(title, url, file)

        self.groups: List[str] = []
        self.campaigns: List[MCCampaign] = []
        self.derivations: List[str] = []

    @classmethod
    def from_dict(cls, dict: Dict[str, Any]) -> 'SampleGroup':
        """Initialize sample group from a dictionary."""
        group = cls(dict['title'], dict['url'], Path(dict['file']))
        group.errors = dict['errors']
        group.groups = dict['groups']
        group.campaigns = [MCCampaign.from_string(c) for c in dict['campaigns']]
        group.derivations = dict['derivations']
        return group


class SampleGroupTasks:
    """Sample group class for tasks summary."""

    def __init__(self, title: str, url: str, file: Path) -> None:
        """Initialize sample group for tasks summary."""
        self.title: str = title
        self.url: str = url
        self.file: Path = file
        self.errors: bool = False

        self.percentage: int = 0
        self.failed: bool = False
        self.broken: bool = False
        self.missing: bool = False

    @classmethod
    def from_dict(cls, dict: Dict[str, Any]) -> 'SampleGroupTasks':
        """Initialize sample group for tasks summary from a dictionary."""
        group = cls(dict['title'], dict['url'], Path(dict['file']))
        group.errors = dict['errors']
        if 'percentage' in dict:
            group.percentage = dict['percentage']
        if 'failed' in dict:
            group.failed = dict['failed']
        if 'broken' in dict:
            group.broken = dict['broken']
        if 'missing' in dict:
            group.missing = dict['missing']
        return group
