"""Samples lists."""
from pathlib import Path
from typing import List, Optional

from ..common import Summary
from ..config import APP_DATA_PATH

from .types import SampleGroup, SampleGroupBase


def get_samples_meta(summary: Optional[Summary] = None) -> List[SampleGroup]:
    """Get meta samples."""
    infolist = []

    sample_mc = SampleGroup('MC', 'MC', Path())
    infolist.append(sample_mc)

    return infolist


def get_samples_mc(summary: Optional[Summary] = None) -> List[SampleGroup]:
    """Get MC samples."""
    path = APP_DATA_PATH / 'samples'
    infolist = []

    for file in path.iterdir():
        if (
            not file.is_file()
            or '.json' not in file.name
            or 'Data' in file.name
            or 'signal' in file.name
        ):
            continue

        basename = file.stem
        title = basename[0].capitalize() + basename[1:]
        title = title.replace('__', '+')
        title_split = title.split('_')
        if len(title_split) > 1:
            title = title_split[0] + ' (' + ', '.join(title_split[1:]) + ')'
        url = basename

        sample = SampleGroup(title, url, file)
        if summary and summary.samples:
            sample.errors = summary.samples[sample.url].errors
        infolist.append(sample)

    infolist.sort(key=lambda x: x.title)

    return infolist


def get_samples_data(summary: Optional[Summary] = None) -> List[SampleGroup]:
    """Get data samples."""
    path = APP_DATA_PATH / 'samples'
    infolist = []

    for file in path.iterdir():
        if not file.is_file() or file.suffix != '.json' or 'Data' not in file.name:
            continue

        basename = file.stem
        title = basename[0].capitalize() + basename[1:]
        title = title.replace('__', '+')
        url = basename

        sample = SampleGroup(title, url, file)
        if summary and summary.samples:
            sample.errors = summary.samples[sample.url].errors
        infolist.append(sample)

    infolist.sort(key=lambda x: x.title)

    return infolist


def get_samples_signal(summary: Optional[Summary] = None) -> List[SampleGroup]:
    """Get signal samples."""
    path = APP_DATA_PATH / 'samples'
    infolist = []

    for file in path.iterdir():
        if not file.is_file() or file.suffix != '.json' or 'signal' not in file.name:
            continue

        basename = file.stem
        title = basename[0].capitalize() + basename[1:]
        title = title.replace('__', '+')
        title = title.replace('_signal', '')
        title_split = title.split('_')
        if len(title_split) > 1:
            title = title_split[0] + ' (' + ', '.join(title_split[1:]) + ')'
        url = basename

        sample = SampleGroup(title, url, file)
        if summary and summary.samples:
            sample.errors = summary.samples[sample.url].errors
        infolist.append(sample)

    infolist.sort(key=lambda x: x.title)

    return infolist


def get_sample_group_info(url: str, tasks: bool = False) -> Optional[SampleGroup]:
    """Find sample group by URL."""
    if tasks:
        meta = get_samples_meta()
        for item in meta:
            if item.url == url:
                return item

    mcs = get_samples_mc()
    data = get_samples_data()
    signal = get_samples_signal()

    for item in mcs:
        if item.url == url:
            return item

    for item in data:
        if item.url == url:
            return item

    for item in signal:
        if item.url == url:
            return item

    return None


def get_sample_group_list(
    group: SampleGroupBase, subset: str, derivation: str, summary: Summary
) -> List[str]:
    """Get a list of types in a group."""
    if group.url == 'Data':
        return [
            'Data_15',
            'Data_16',
            'Data_17',
            'Data_18',
        ]

    if group.url == 'MC':
        samples = get_samples_mc()
        url_list = []

        for s in samples:
            summary_info = summary.samples[s.url]

            if derivation not in summary_info.derivations:
                continue

            if summary_info.groups and subset not in summary_info.groups:
                continue

            for c in summary_info.campaigns:
                if 'fast' in s.url:
                    url_list.append(s.url.replace('__', '') + f'_{c.value}_fast')
                else:
                    url_list.append(s.url.replace('__', '') + f'_{c.value}')

        return url_list

    url_list = []

    summary_info = summary.samples[group.url]

    if derivation not in summary_info.derivations:
        return []

    for c in summary_info.campaigns:
        if 'signal' in group.url:
            url_list.append(
                group.url.replace('__', '').replace('_signal', '') + f'_{c.value}_fast'
            )
        else:
            url_list.append(group.url.replace('__', '') + f'_{c.value}')

    return url_list
