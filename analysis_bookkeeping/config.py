"""Config helpers."""

from pathlib import Path
from os import getenv
from yaml import FullLoader, load

SECRET = getenv('BOOKKEEPING_SECRET', '1234567890')

CERN_CLIENT_ID = getenv('BOOKKEEPING_OAUTH_ID')
CERN_CLIENT_SECRET = getenv('BOOKKEEPING_OAUTH_SECRET')

LOGIN_REQUIRED = str(getenv('BOOKKEEPING_REQUIRES_LOGIN')).lower() in [
    'yes',
    'true',
    't',
    '1',
]

APPLICATION_ROOT = getenv('BOOKKEEPING_ROOT', '/')

DATA_PATH = Path(getenv('BOOKKEEPING_DATA_PATH', '../analysis-data'))
CONFIG_PATH = DATA_PATH / 'config.yaml'
SAMPLE_DATABASE_PATH = DATA_PATH / 'SampleDatabase' / 'Database'
APP_DATA_PATH = DATA_PATH / 'app_data'
PLOTS_PATH = APP_DATA_PATH / 'plots'
BIGPANDA = 'https://bigpanda.cern.ch'

with open(DATA_PATH / 'config.yaml', 'r') as f:
    APPLICATION_CONFIG = load(f, Loader=FullLoader)
