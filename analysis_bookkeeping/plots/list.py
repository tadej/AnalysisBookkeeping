"""Plots listing helpers."""
from json import load
from typing import List

from ..config import PLOTS_PATH
from .types import (
    Histogram,
    PlotsConfiguration,
    PlotsConfigurationMetadata,
    PlotsSummary,
)


def load_plots_summary() -> PlotsSummary:
    """Load plots summary."""
    file = PLOTS_PATH / 'groups.json'
    with file.open('r') as f:
        return PlotsSummary.from_dict(load(f))


def get_histograms(configuration: PlotsConfiguration) -> List[Histogram]:
    """Get a list of histograms for a configuration."""
    with configuration.file.open('r') as f:
        data = load(f)

    histograms: List[Histogram] = []
    for h in data['histograms']:
        histograms.append(Histogram(h, configuration.file.parent))
    return histograms


def get_metadata(configuration: PlotsConfiguration) -> PlotsConfigurationMetadata:
    """Get metadata for a configuration."""
    with configuration.file.open('r') as f:
        data = load(f)

    if 'metadata' not in data:
        return PlotsConfigurationMetadata({})

    return PlotsConfigurationMetadata(data['metadata'])
