"""Plots bookkeeping types."""
from pathlib import Path
from typing import Any, Dict, List, Optional

from ..config import PLOTS_PATH


class PlotsGroup:
    """Plots group class."""

    def __init__(self, campaign: str, name: str) -> None:
        """Initialize plots group."""
        self.title: str = f"{name.replace('_', '+')} ({campaign.replace('_', ' ')})"
        self.campaign: str = campaign
        self.name: str = name

    def web_id(self) -> str:
        """Get web ID for a group."""
        return f'{self.campaign}__{self.name}'

    @classmethod
    def from_dict(cls, dict: Dict[str, Any]) -> 'PlotsGroup':
        """Initialize plots group from a dictionary."""
        return cls(dict['campaign'], dict['name'])


class PlotsRevision:
    """Plots revision class."""

    def __init__(self, label: str, date: str, group: PlotsGroup) -> None:
        """Initialize plots revision."""
        self.label: str = label.replace('_', ' ')
        self.date: str = date
        self.group: PlotsGroup = group

    def web_id(self) -> str:
        """Get web ID for a revision."""
        label = self.label.replace(' ', '_')
        return f'{self.date}__{label}'

    @classmethod
    def from_dict(
        cls, dict: Dict[str, Any], groups: Optional[List[PlotsGroup]] = None
    ) -> 'PlotsRevision':
        """Initialize plots revision from a dictionary."""
        group = PlotsGroup.from_dict(dict['group'])
        if groups:
            for g in groups:
                if g.title == group.title:
                    group = g
                    break

        return cls(dict['label'], dict['date'], group)


class PlotsConfiguration:
    """Plots configuration class."""

    def __init__(
        self,
        configuration: str,
        variation: str,
        variation_label: str,
        channel: str,
        file: Path,
        revision: PlotsRevision,
    ) -> None:
        """Initialize plots configuration."""
        self.configuration: str = configuration
        self.variation: str = variation
        self.variation_label: str = variation_label
        self.channel: str = channel
        self.file: Path = file
        self.revision: PlotsRevision = revision

    def __str__(self) -> str:
        """Representation configuration as string."""
        label = self.variation_label if self.variation_label else self.variation
        return f'{self.configuration} ({label}, {self.channel})'

    def web_id(self) -> str:
        """Get web ID for a configuration."""
        return self.file.stem

    @classmethod
    def from_dict(
        cls,
        dict: Dict[str, Any],
        revisions: Optional[List[PlotsRevision]] = None,
        groups: Optional[List[PlotsGroup]] = None,
    ) -> 'PlotsConfiguration':
        """Initialize plots configuration from a dictionary."""
        revision = PlotsRevision.from_dict(dict['revision'], groups)
        if revisions:
            for r in revisions:
                if (
                    r.group == revision.group
                    and r.date == revision.date
                    and r.label == revision.label
                ):
                    revision = r
                    break

        return cls(
            dict['configuration'],
            dict['variation'],
            dict['variation_label'],
            dict['channel'],
            Path(dict['file']),
            revision,
        )


class PlotsSummary:
    """Plots summary class."""

    def __init__(self) -> None:
        """Initialize plots summary."""
        self.groups: List[PlotsGroup] = []
        self.revisions: List[PlotsRevision] = []
        self.configurations: List[PlotsConfiguration] = []

    @classmethod
    def from_dict(cls, dict: Dict[str, Any]) -> 'PlotsSummary':
        """Initialize plots summary from a dictionary."""
        summary = cls()
        summary.groups = [PlotsGroup.from_dict(d) for d in dict['groups']]
        summary.revisions = [
            PlotsRevision.from_dict(d, summary.groups) for d in dict['revisions']
        ]
        summary.configurations = [
            PlotsConfiguration.from_dict(d, summary.revisions, summary.groups)
            for d in dict['configurations']
        ]
        return summary


class Histogram:
    """Histogram metadata class."""

    def __init__(self, dict: Dict[str, Any], path: Path) -> None:
        """Initialize histogram metadata."""
        self.name: str = dict['name']

        self.path_png: Path = path / f'{self.name}.png'
        self.path_pdf: Path = path / f'{self.name}.pdf'
        self.plot_png: Path = self.path_png.relative_to(PLOTS_PATH)
        self.plot_pdf: Path = self.path_pdf.relative_to(PLOTS_PATH)


class SelectionCut:
    """Selection cut class."""

    def __init__(self, dict: Dict[str, Any]) -> None:
        """Initialize selection cut."""
        self.quantity: str = dict['quantity']
        self.label: str = dict['label']
        self.lower: float = dict['lower']
        self.upper: float = dict['upper']
        self.inversed: bool = dict['inversed']

    def key(self) -> str:
        """Get key friendly string."""
        return self.label.replace(' ', '&nbsp;')

    def value(self) -> str:
        """Get string representation of the value."""
        geq = r'\(\geq\)'
        low = r'\(<\)'
        equ = r'\(=\)'
        if self.upper > 1e100:
            return (
                f'{geq}&nbsp;{self.lower:.0f}'
                if self.lower.is_integer()
                else f'{geq}&nbsp;{self.lower}'
            )
        if self.lower < -1e100:
            return (
                f'{low}&nbsp;{self.upper:.0f}'
                if self.upper.is_integer()
                else f'{low}&nbsp;{self.upper}'
            )
        if self.lower == self.upper:
            return (
                f'{equ}&nbsp;{self.lower:.0f}'
                if self.lower.is_integer()
                else f'{equ}&nbsp;{self.lower}'
            )
        if self.inversed:
            return f'{low}&nbsp;{self.lower} & {geq} {self.upper}'
        return f'{geq}&nbsp;{self.lower} & {low}&nbsp;{self.upper}'


class ObjectDefinitions:
    """Object definitions class."""

    def __init__(self, dict: Dict[str, Any]) -> None:
        """Initialize object definitions."""
        self.jet_pt_min: float = dict['jet_pt_min']
        self.jet_eta_max: float = dict['jet_eta_max']
        self.electron_pt_min: float = dict['electron_pt_min']
        self.electron_ID_tight: str = dict['electron_ID_tight']
        self.electron_ID_loose: Optional[str] = (
            dict['electron_ID_loose']
            if 'electron_ID_loose' in dict
            and self.electron_ID_tight != dict['electron_ID_loose']
            else None
        )
        self.electron_isolation: str = dict['electron_isolation']
        self.muon_pt_min: str = dict['muon_pt_min']
        self.muon_reco_tight: str = dict['muon_reco_tight']
        self.muon_reco_loose: Optional[str] = (
            dict['muon_reco_loose']
            if 'muon_reco_loose' in dict
            and self.muon_reco_tight != dict['muon_reco_loose']
            else None
        )
        self.muon_isolation: str = dict['muon_isolation']
        self.flavour_tagging: Optional[str] = (
            dict['flavour_tagging'] if 'flavour_tagging' in dict else None
        )
        self.hadronic_W: Optional[str] = (
            dict['hadronic_W'] if 'hadronic_W' in dict else None
        )

    def key(self, key_str: str) -> str:
        """Get key friendly string."""
        out_str = ''
        if key_str == 'jet_pt_min':
            out_str = r'Minimum jet \(p_\mathrm{T}\)'
        elif key_str == 'jet_eta_max':
            out_str = r'Maximum jet \(\eta\)'
        elif key_str == 'electron_pt_min':
            out_str = r'Minimum electron \(p_\mathrm{T}\)'
        elif key_str == 'electron_ID_tight':
            out_str = 'Signal electron ID'
        elif key_str == 'electron_ID_loose':
            out_str = 'Baseline electron ID'
        elif key_str == 'muon_pt_min':
            out_str = r'Minimum muon \(p_\mathrm{T}\)'
        elif key_str == 'muon_reco_tight':
            out_str = 'Signal muon quality'
        elif key_str == 'muon_reco_loose':
            out_str = 'Baseline muon quality'
        elif key_str == 'flavour_tagging':
            out_str = 'Flavour tagging'
        elif key_str == 'hadronic_W':
            out_str = r'Hadronic \(W\) reconstruction'
        return out_str.replace(' ', '&nbsp;')

    def value(self, key_str: str) -> str:
        """Get string representation of the value."""
        value = self.__dict__[key_str]
        if type(value) == float:
            return f'{value:.0f}' if value.is_integer() else f'{value}'
        return str(value).replace('_', ' ')


class PlotsConfigurationMetadata:
    """Plots configuration metadata."""

    def __init__(self, dict: Dict[str, Any]) -> None:
        """Initialize plots configuration metadata."""
        self.object_definitions: Optional[ObjectDefinitions] = (
            ObjectDefinitions(dict['object_definition'])
            if 'object_definition' in dict
            else None
        )
        self.selection: List[SelectionCut] = (
            [SelectionCut(d) for d in dict['selection']] if 'selection' in dict else []
        )
