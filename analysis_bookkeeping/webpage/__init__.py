"""Webpage renderers."""
from typing import Union
from werkzeug import Response

WebpageResponse = Union[Response, str]

__all__ = ['WebpageResponse']
