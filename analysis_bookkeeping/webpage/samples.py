"""Pages implementation for the samples bookkeeping."""
from flask import g, redirect, render_template, url_for

from . import WebpageResponse
from ..common import get_updated_time, load_summary
from ..samples.list import (
    get_samples_data,
    get_samples_mc,
    get_samples_signal,
    get_sample_group_info,
)
from ..samples.info import get_samples, check_derivations


def page_samples() -> str:
    """Render samples main page."""
    g.title = 'Samples Info'
    g.id = 'samples'
    g.updated = get_updated_time()

    summary = load_summary()

    samples_data = get_samples_data(summary)
    samples_mc = get_samples_mc(summary)
    samples_signal = get_samples_signal(summary)

    return render_template(
        'samples.html',
        samples_data=samples_data,
        samples_signal=samples_signal,
        samples_mc=samples_mc,
    )


def page_samples_list(in_group: str) -> WebpageResponse:
    """List samples for one group."""
    group = get_sample_group_info(in_group)
    if not group:
        return redirect(url_for('samples'))

    g.title = group.title + ' Samples List'
    g.id = 'samples'
    g.updated = get_updated_time()

    samples = get_samples(group.file)

    available, missing = check_derivations(samples)

    return render_template(
        'samples_list.html',
        samples=samples,
        derivations_available=available,
        derivations_missing=missing,
    )
