"""Pages implementation for the tasks bookkeeping."""
from flask import g, redirect, render_template, url_for

from . import WebpageResponse
from ..common import get_updated_time, load_summary
from ..samples.list import (
    get_sample_group_info,
    get_sample_group_list,
)
from ..tasks.info import (
    get_campaigns_summary,
    get_campaign_info,
    load_report,
    process_report,
)


def page_tasks() -> WebpageResponse:
    """Render main tasks page."""
    g.title = 'Tasks Info'
    g.id = 'tasks'
    g.updated = get_updated_time()

    summary = load_summary()
    if not summary:
        return redirect(url_for('index'))

    (campaigns, campaigns_info) = get_campaigns_summary(summary)

    return render_template(
        'tasks.html', campaigns=campaigns, campaigns_info=campaigns_info
    )


def page_tasks_list(in_campaign: str, in_group: str) -> WebpageResponse:
    """List tasks for one group."""
    summary = load_summary()
    if not summary:
        return redirect(url_for('index'))

    campaign = get_campaign_info(in_campaign)
    if not campaign:
        return redirect(url_for('tasks'))

    group = get_sample_group_info(in_group, True)
    if not group:
        return redirect(url_for('tasks'))

    g.title = campaign.title + group.title
    g.id = 'tasks'
    g.updated = get_updated_time()

    dsids = get_sample_group_list(group, campaign.name, campaign.derivation, summary)
    if not dsids:
        return redirect(url_for('tasks'))

    report = load_report(campaign.file)
    if not report:
        return redirect(url_for('tasks'))

    (
        events_total,
        events_done,
        done_list,
        not_done_list,
        has_failed_list,
        broken_list,
        not_found_list,
        superseded,
    ) = process_report(report, dsids)

    percentage = int(100 * events_done / events_total) if events_total else 0

    return render_template(
        'tasks_list.html',
        in_campaign=in_campaign,
        in_group=in_group,
        percentage=percentage,
        dsids=dsids,
        done_list=done_list,
        not_done_list=not_done_list,
        has_failed_list=has_failed_list,
        broken_list=broken_list,
        not_found_list=not_found_list,
        superseded=superseded,
        events_total=events_total,
        events_remaining=events_total - events_done,
        events_done=events_done,
    )


def page_tasks_download(in_campaign: str, in_group: str) -> WebpageResponse:
    """Download tasks for one group."""
    summary = load_summary()
    if not summary:
        return redirect(url_for('index'))

    campaign = get_campaign_info(in_campaign)
    if not campaign:
        return redirect(url_for('tasks'))

    group = get_sample_group_info(in_group, True)
    if not group:
        return redirect(url_for('tasks'))

    g.title = campaign.title + group.title
    g.id = 'tasks'

    dsids = get_sample_group_list(group, campaign.name, campaign.derivation, summary)
    if not dsids:
        return redirect(url_for('tasks'))

    report = load_report(campaign.file)
    if not report:
        return redirect(url_for('tasks'))

    _, _, done_list, _, _, _, _, _ = process_report(report, dsids)

    return render_template('tasks_download.html', done_list=done_list)


def page_tasks_download_metadata(in_campaign: str, in_group: str) -> WebpageResponse:
    """Download tasks for one group - only metadata."""
    summary = load_summary()
    if not summary:
        return redirect(url_for('index'))

    campaign = get_campaign_info(in_campaign)
    if not campaign:
        return redirect(url_for('tasks'))

    group = get_sample_group_info(in_group, True)
    if not group:
        return redirect(url_for('tasks'))

    g.title = campaign.title + group.title
    g.id = 'tasks'

    dsids = get_sample_group_list(group, campaign.name, campaign.derivation, summary)
    if not dsids:
        return redirect(url_for('tasks'))

    report = load_report(campaign.file)
    if not report:
        return redirect(url_for('tasks'))

    _, _, done_list, _, _, _, _, _ = process_report(report, dsids)

    return render_template('tasks_download_metadata.html', done_list=done_list)


def page_tasks_replicate(
    in_campaign: str, in_group: str, in_rse: str
) -> WebpageResponse:
    """Replicate tasks for one group."""
    summary = load_summary()
    if not summary:
        return redirect(url_for('index'))

    campaign = get_campaign_info(in_campaign)
    if not campaign:
        return redirect(url_for('tasks'))

    group = get_sample_group_info(in_group, True)
    if not group:
        return redirect(url_for('tasks'))

    g.title = campaign.title + group.title
    g.id = 'tasks'

    dsids = get_sample_group_list(group, campaign.name, campaign.derivation, summary)
    if not dsids:
        return redirect(url_for('tasks'))

    report = load_report(campaign.file)
    if not report:
        return redirect(url_for('tasks'))

    _, _, done_list, not_done_list, _, _, _, _ = process_report(report, dsids)

    task_list = done_list[:]
    for task in not_done_list:
        if task.status in ['finished', 'running']:
            task_list.append(task)

    task_list.sort(key=lambda x: x.name)

    return render_template('tasks_replicate.html', task_list=task_list, rse=in_rse)
