"""Pages implementation for the plots bookkeeping."""
from flask import g, redirect, render_template, send_from_directory, url_for
from pathlib import Path
from typing import Dict, List

from . import WebpageResponse
from ..config import PLOTS_PATH
from ..common import get_updated_time
from ..plots import (
    get_histograms,
    get_metadata,
    load_plots_summary,
    PlotsGroup,
    PlotsRevision,
    PlotsConfiguration,
)


def page_plots() -> WebpageResponse:
    """Render main plots page."""
    g.title = 'Plots & Distributions'
    g.id = 'plots'
    g.updated = get_updated_time()

    summary = load_plots_summary()
    if not summary:
        return redirect(url_for('index'))

    revs: Dict[PlotsGroup, List[PlotsRevision]] = {}
    configs: Dict[PlotsRevision, List[PlotsConfiguration]] = {}
    for group in summary.groups:
        revs[group] = []
    for revision in summary.revisions:
        revs[revision.group].append(revision)
        configs[revision] = []
    for config in summary.configurations:
        configs[config.revision].append(config)
    for revision in summary.revisions:
        configs[revision].sort(key=lambda x: x.__str__())

    return render_template(
        'plots.html',
        groups=summary.groups,
        revisions=revs,
        configurations=configs,
    )


def page_plots_list(in_group: str, in_revision: str, in_config: str) -> WebpageResponse:
    """List plots for one configuration."""
    summary = load_plots_summary()
    if not summary:
        return redirect(url_for('index'))

    configuration = None
    for config in summary.configurations:
        if config.revision.group.web_id() != in_group:
            continue
        if config.revision.web_id() != in_revision:
            continue
        if config.web_id() != in_config:
            continue
        configuration = config

    if not configuration:
        return redirect(url_for('plots'))

    g.title = f'{configuration.__str__()} - Plots & Distributions'
    g.id = 'plots'
    g.updated = get_updated_time()

    metadata = get_metadata(configuration)
    histograms = get_histograms(configuration)

    return render_template(
        'plots_list.html',
        title=configuration.__str__(),
        histograms=histograms,
        metadata=metadata,
    )


def send_plot(filename: str) -> WebpageResponse:
    """Send plot."""
    file = PLOTS_PATH / filename
    if not file.exists():
        return redirect(url_for('plots'))

    if not file.is_absolute():
        file = Path.cwd() / file

    return send_from_directory(file.parent, file.name)
