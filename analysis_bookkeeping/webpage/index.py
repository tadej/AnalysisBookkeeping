"""Index webpage."""
from flask import g, render_template

from ..common.cache import get_updated_time


def page_index() -> str:
    """Index webpage."""
    g.title = 'Home'
    g.id = 'index'
    g.updated = get_updated_time()

    return render_template('index.html')
