"""Status summary."""
from json import load
from time import time
from typing import Dict, Optional

from ..config import APP_DATA_PATH
from ..samples import SampleGroup
from ..tasks import CampaignSubset


class Summary:
    """Status summary class."""

    def __init__(self) -> None:
        """Initialize status summary."""
        self.updated: float = time()
        self.samples: Dict[str, SampleGroup] = {}
        self.tasks: Dict[str, CampaignSubset] = {}


def load_summary() -> Optional[Summary]:
    """Load status summary from file."""
    file = APP_DATA_PATH / 'summary.json'

    try:
        with open(file, 'r') as f:
            data = load(f)
    except IOError:
        return None

    if not data:
        return None

    summary = Summary()
    summary.updated = data['updated']
    for url, group in data['samples'].items():
        summary.samples[url] = SampleGroup.from_dict(group)
    for url, subset in data['tasks'].items():
        summary.tasks[url] = CampaignSubset.from_dict(subset)
    return summary
