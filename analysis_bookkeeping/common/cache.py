"""Cache helper functions."""
from datetime import datetime
from enum import Enum
from json import dump, dumps
from pathlib import Path
from time import time
from typing import Any, Optional

from ..config import APP_DATA_PATH

from .summary import Summary, load_summary


def converter(content: Any) -> Any:
    """Convert to JSON."""
    if isinstance(content, Path):
        return str(content)
    if isinstance(content, Enum):
        return content.value
    return content.__dict__


def get_updated_time(summary: Optional[Summary] = None) -> str:
    """Get updated time string from the summary file."""
    if not summary:
        summary = load_summary()

    if not summary or not summary.updated:
        return 'n/a'

    date = datetime.fromtimestamp(summary.updated)
    return date.strftime('%Y-%m-%d %H:%M:%S')


def update_timestamp() -> None:
    """Update summary timestamp."""
    file = APP_DATA_PATH / 'summary.json'

    summary = load_summary()
    if not summary:
        summary = Summary()
    else:
        summary.updated = time()

    with file.open('w') as output:
        dump(summary, output, indent=2, default=converter)


def check_and_write(file: Path, content: Any) -> None:
    """Check file for changes, then write it."""
    try:
        file.parent.mkdir(parents=True, exist_ok=True)
        with file.open('r') as output_old:
            old_content = output_old.read()

        new_content = dumps(content, indent=2, default=converter)

        if old_content == new_content:
            print(f"Content of '{file}' did not change.")
            changed = False
        else:
            changed = True
    except IOError:
        changed = True

    if changed:
        print(f"Writing '{file}'...")

        with file.open('w') as output:
            dump(content, output, indent=2, default=converter)

        update_timestamp()
