"""Analysis bookkeeping."""

from .bookkeeping import application as app

__all__ = ['app']
__version__ = '0.1.0'
