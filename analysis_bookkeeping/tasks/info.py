"""Tasks info helpers."""

from json import load
from pathlib import Path
from typing import List, Optional, Tuple

from ..common import Summary, load_summary
from ..config import APP_DATA_PATH
from ..samples import SampleGroupTasks

from .types import CampaignInfo, CampaignSubset, Task, TasksReport


def campaign_file_name(campaign: str) -> str:
    """Generate campaign file name."""
    array = campaign.split('.')
    return f'{array[0]}_{array[1]}__{array[2]}.json'


def load_report(filename: Path, optional: bool = False) -> Optional[TasksReport]:
    """Load tasks report from file."""
    report_input = None
    try:
        with filename.open('r') as f:
            report_input = load(f)
    except IOError as e:
        if not optional:
            raise IOError(e)

    if not report_input:
        if optional:
            return None
        else:
            raise ValueError('Invalid tasks report')

    report: TasksReport = {}
    for sample, inputs in report_input.items():
        tasks = []
        for task in inputs:
            tasks.append(Task(task, sample))
        report[sample] = tasks

    return report


def get_campaigns_summary(summary: Summary) -> Tuple[List[str], CampaignInfo]:
    """Get campaigns summary."""
    path = APP_DATA_PATH / 'tasks'

    campaigns: List[str] = []
    campaigns_info: CampaignInfo = {}

    for file in path.iterdir():
        if not file.is_file() or file.suffix != '.json':
            continue

        campaign = file.stem.split('__')[0]
        subset = file.stem.split('__')[1]
        derivation = get_subset_derivation(subset)

        if campaign not in campaigns:
            campaigns.append(campaign)
            campaigns_info[campaign] = []

        samples = []
        if not summary.tasks or file.stem not in summary.tasks:
            has_mc = False
            for s, sample in summary.samples.items():
                if sample.groups and subset not in sample.groups:
                    continue

                if derivation in sample.derivations:
                    if 'signal' in s or 'Data' in s:
                        samples.append(SampleGroupTasks.from_dict(sample.__dict__))
                    else:
                        has_mc = True
            if has_mc:
                samples.append(SampleGroupTasks('MC', 'MC', Path()))
        else:
            for sample_task in summary.tasks[file.stem].samples:
                if (
                    sample_task.url != 'MC'
                    and summary.samples[sample_task.url].groups
                    and subset not in summary.samples[sample_task.url].groups
                ):
                    continue

                samples.append(SampleGroupTasks.from_dict(sample_task.__dict__))

        samples.sort(key=lambda x: x.title)

        campaign_subset = CampaignSubset(campaign, subset, file, derivation)
        campaign_subset.samples = samples
        campaigns_info[campaign].append(campaign_subset)

    for campaign in campaigns:
        campaigns_info[campaign].sort(key=lambda x: x.name)

    campaigns.sort(reverse=True)

    return (campaigns, campaigns_info)


def get_campaign_info(campaign: str) -> Optional[CampaignSubset]:
    """Find campaign info by URL."""
    summary = load_summary()
    if not summary:
        raise RuntimeError('Undefined behaviour')

    (_, campaigns_info) = get_campaigns_summary(summary)

    for item in campaigns_info:
        for subset in campaigns_info[item]:
            if subset.url == campaign:
                return subset

    return None


def process_report(
    report: TasksReport, dsids: List[str]
) -> Tuple[
    int, int, List[Task], List[Task], List[Task], List[Task], List[str], List[Task]
]:
    """Process tasks report for a list of dsids."""
    done_list = []
    not_done_list = []
    has_failed_list = []
    broken_list = []
    superseded = []

    events_done: int = 0
    events_total: int = 0

    not_found_list = []

    for key in report:
        if key not in dsids:
            continue

        latest = []

        for task in report[key]:
            if key not in latest:
                latest.append(key)

                if task.status == 'done':
                    done_list.append(task)
                else:
                    not_done_list.append(task)
                    if (
                        task.failed_files > 0 and task.status != 'broken'
                    ) or task.status == 'aborted':
                        has_failed_list.append(task)
                    if task.status == 'broken':
                        broken_list.append(task)

                events_done += task.total_events - task.remaining_events
                events_total += task.total_events
            else:
                superseded.append(task)

    for dsid in dsids:
        if dsid not in report.keys():
            not_found_list.append(dsid)

    done_list.sort(key=lambda x: x.name)
    not_done_list.sort(key=lambda x: x.name)
    has_failed_list.sort(key=lambda x: x.name)
    broken_list.sort(key=lambda x: x.name)
    superseded.sort(key=lambda x: x.name)
    not_found_list.sort()

    return (
        events_total,
        events_done,
        done_list,
        not_done_list,
        has_failed_list,
        broken_list,
        not_found_list,
        superseded,
    )


def get_subset_derivation(subset: str) -> str:
    """Get a derivation for subset."""
    # TODO: make configurable
    return 'PHYS'
