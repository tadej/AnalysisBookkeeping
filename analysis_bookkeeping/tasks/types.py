"""Task information types."""
from pathlib import Path
from typing import Any, Dict, List

from ..samples import SampleGroupTasks

PandaResponse = List[Dict[str, Any]]


class Task:
    """Task info."""

    def __init__(self, task_dict: Dict[str, Any], sample: str) -> None:
        """Initialize task info."""
        if 'parent_tid' in task_dict:
            self.tid: int = int(task_dict['parent_tid'])
            self.name: str = task_dict['taskname'][:-1]
            self.sample: str = sample
            self.status: str = task_dict['status']
            self.date: str = task_dict['creationdate']
            self.total_events: int = int(task_dict['totev'])
            self.remaining_events: int = int(task_dict['totevrem'])
            self.files: int = int(task_dict['nfiles'])
            self.finished_files: int = int(task_dict['nfilesfinished'])
            self.failed_files: int = int(task_dict['nfilesfailed'])
        else:
            self.tid = int(task_dict['tid'])
            self.name = task_dict['name']
            self.sample = sample
            self.status = task_dict['status']
            self.date = task_dict['date']
            self.total_events = int(task_dict['total_events'])
            self.remaining_events = int(task_dict['remaining_events'])
            self.files = int(task_dict['files'])
            self.finished_files = int(task_dict['finished_files'])
            self.failed_files = int(task_dict['failed_files'])


TasksReport = Dict[str, List[Task]]


class CampaignSubset:
    """Campaign subset class."""

    def __init__(self, campaign: str, name: str, file: Path, derivation: str) -> None:
        """Initialize campaign subset."""
        self.title: str = f"{campaign.replace('_', ' ')}: {name.replace('_', '+')} for "
        self.campaign: str = campaign
        self.name: str = name
        self.url: str = file.stem
        self.samples: List[SampleGroupTasks] = []
        self.derivation: str = derivation
        self.file: Path = file

    @classmethod
    def from_dict(cls, dict: Dict[str, Any]) -> 'CampaignSubset':
        """Initialize campaign subset from a dictionary."""
        subset = cls(
            dict['campaign'], dict['name'], Path(dict['file']), dict['derivation']
        )
        subset.samples = [SampleGroupTasks.from_dict(s) for s in dict['samples']]
        return subset


CampaignInfo = Dict[str, List[CampaignSubset]]
