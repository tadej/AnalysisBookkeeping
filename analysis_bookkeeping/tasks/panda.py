"""Panda helpers."""

import re

from json import loads
from typing import List, Optional, Set, cast
from urllib.request import urlopen

from .types import PandaResponse, Task, TasksReport


def get_tasks(username: str, analysis: str, filter: str = '') -> PandaResponse:
    """Get tasks from server."""
    days = 5
    display_limit = 1000
    taskname = '*' + analysis + '.*'
    if filter:
        taskname = '*' + analysis + '.' + filter + '.*'

    url = (
        f'http://bigpanda.cern.ch/tasks/?username={username}'
        f'&days={days}&display_limit={display_limit}'
        f'&taskname={taskname}&json'
    )

    urlhandle = urlopen(url)
    page = urlhandle.read()

    return cast(PandaResponse, loads(page))


def get_campaigns(tasks: PandaResponse) -> Set[str]:
    """Get campaigns from a list of tasks."""
    campaigns = set()
    for task in tasks:
        search = re.search(
            '[a-z]+.[a-z]+.([a-zA-Z]+.v[0-9]+.[a-zA-Z]+)', task['taskname']
        )
        if not search:
            continue
        campaign = search.group(1)
        campaigns.add(campaign)
    return campaigns


def get_report(
    tasks: PandaResponse, campaign: str, old_report: Optional[TasksReport] = None
) -> TasksReport:
    """Get tasks report for a specific campaign."""
    report: TasksReport = old_report if old_report else {}

    print(f'Processing {len(tasks)} tasks...')

    for task in reversed(tasks):
        campaign_check = campaign + '.'
        if campaign_check not in task['taskname']:
            continue

        if not task['datasets']:
            continue

        search_sample = re.search(
            '[a-z]+.[a-z]+.[a-zA-Z]+.v[0-9]+.[a-zA-Z]+.([a-zA-Z_0-9]+)',
            task['taskname'],
        )
        if not search_sample:
            continue

        sample = search_sample.group(1)
        if sample not in report.keys():
            report[sample] = []

        find_and_remove_task(report[sample], int(task['parent_tid']))

        report[sample].append(Task(task, sample))

    for sample in report:
        report[sample].sort(key=lambda x: x.date, reverse=True)

    return report


def find_and_remove_task(array: List[Task], tid: int) -> None:
    """Find and remove the task in a report array."""
    for i, task in enumerate(array):
        if task.tid == tid:
            del array[i]
            break
