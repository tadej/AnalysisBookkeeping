"""The plots CLI group."""
import click
from json import load

from ..config import PLOTS_PATH
from ..common.cache import check_and_write
from ..plots.types import PlotsConfiguration, PlotsGroup, PlotsRevision, PlotsSummary


@click.group(name='plots')
def cli() -> None:
    """Run the plots CLI group."""


@cli.command()
def plots() -> None:
    """Generate plots info."""
    print('### Generating plots info')
    print('')

    path_out = PLOTS_PATH / 'groups.json'

    print(f'Looking for plots in: {PLOTS_PATH}')
    print(f'Summary output in: {path_out}')
    print('')

    output = PlotsSummary()

    groups = [d for d in PLOTS_PATH.iterdir() if d.is_dir()]
    for g in groups:
        name_list = g.stem.split('__')
        group = PlotsGroup(name_list[0], name_list[1])
        output.groups.append(group)
        print(f'## {group.title}')

        revs = [d for d in g.iterdir() if d.is_dir()]
        for r in revs:
            name_list = r.stem.split('__')
            revision = PlotsRevision(name_list[1], name_list[0], group)
            output.revisions.append(revision)
            print(f'# {revision.label} ({revision.date}')

            dirs = [d for d in r.iterdir() if d.is_dir()]
            for d in dirs:
                files = [f for f in d.iterdir() if f.is_file() and f.suffix == '.json']
                for file in files:
                    with file.open('r') as f:
                        data = load(f)
                    configuration = PlotsConfiguration(
                        data['configuration'],
                        data['variation'],
                        data['variation_label'],
                        data['channel'],
                        file,
                        revision,
                    )
                    output.configurations.append(configuration)
                    print(configuration)
        print()

    output.groups.sort(key=lambda x: x.title, reverse=True)
    output.revisions.sort(key=lambda x: (x.date, x.label), reverse=True)
    output.configurations.sort(key=lambda x: x.__str__())

    check_and_write(path_out, output)
