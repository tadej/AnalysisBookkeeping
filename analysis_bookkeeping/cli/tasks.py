"""The tasks CLI group."""
import click

from ..config import APP_DATA_PATH, APPLICATION_CONFIG
from ..common.cache import check_and_write
from ..tasks.info import campaign_file_name, load_report
from ..tasks.panda import get_tasks, get_campaigns, get_report


@click.group(name='samples')
def cli() -> None:
    """Run the samples CLI group."""


@cli.command()
def tasks() -> None:
    """Generate tasks info."""
    print('### Generating tasks info')
    print('')

    path_out = APP_DATA_PATH / 'tasks'

    username = APPLICATION_CONFIG['username']
    analyses = APPLICATION_CONFIG['analyses']

    print(f'Data output in: {path_out}')

    for analysis in analyses:
        tasks = get_tasks(username, analysis)
        campaigns = get_campaigns(tasks)

        for campaign in campaigns:
            print(f'# {campaign}')

            filename = path_out / campaign_file_name(campaign)
            old_report = load_report(filename, optional=True)
            report = get_report(tasks, campaign, old_report)

            check_and_write(filename, report)

            print('')
