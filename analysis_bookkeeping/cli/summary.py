"""Summary CLI."""
import click
from json import dump
from time import time

from ..common import Summary, load_summary
from ..common.cache import converter
from ..config import APP_DATA_PATH

from ..samples.generate import generate_groups_summary
from ..samples.info import (
    get_derivations_types,
    get_samples,
    get_samples_campaigns,
    get_samples_groups,
)
from ..samples.list import (
    get_samples_data,
    get_samples_mc,
    get_samples_signal,
    get_sample_group_list,
)
from ..tasks.info import get_campaigns_summary, load_report, process_report


@click.command()
def summary() -> None:
    """Update metadata summary."""
    file = APP_DATA_PATH / 'summary.json'

    summary = load_summary()
    if not summary:
        summary = Summary()
    else:
        summary.updated = time()

    # Samples types
    samples_data = get_samples_data()
    samples_mc = get_samples_mc()
    samples_signal = get_samples_signal()

    samples_all = {}

    groups_summary = generate_groups_summary()

    for sample in samples_data:
        samples_list = get_samples(sample.file)
        sample.errors = False
        for item in samples_list:
            if not item.datasets.DAOD:
                sample.errors = True

        sample.groups = [g for g in get_samples_groups(sample.url, groups_summary)]
        sample.derivations = [d for d in get_derivations_types(samples_list)]

        samples_all[sample.url] = sample

    for sample in samples_mc:
        samples_list = get_samples(sample.file)
        sample.errors = False
        for item in samples_list:
            if (
                not item.metadata
                or item.metadata.total_xsec == -1.0
                or not item.datasets.AOD
                or not item.datasets.DAOD
                or not item.datasets.NTUP_PILEUP
            ):
                sample.errors = True

        sample.campaigns = [c for c in get_samples_campaigns(samples_list)]
        sample.groups = [g for g in get_samples_groups(sample.url, groups_summary)]
        sample.derivations = [d for d in get_derivations_types(samples_list)]

        samples_all[sample.url] = sample

    for sample in samples_signal:
        samples_list = get_samples(sample.file)
        sample.errors = False
        for item in samples_list:
            if (
                not item.metadata
                or item.metadata.total_xsec == -1.0
                or not item.datasets.AOD
                or not item.datasets.DAOD
                or not item.datasets.NTUP_PILEUP
            ):
                sample.errors = True

        sample.campaigns = [c for c in get_samples_campaigns(samples_list)]
        sample.groups = [g for g in get_samples_groups(sample.url, groups_summary)]
        sample.derivations = [d for d in get_derivations_types(samples_list)]

        samples_all[sample.url] = sample

    summary.samples = samples_all

    # Tasks
    subsets = {}
    (campaigns, campaigns_info) = get_campaigns_summary(summary)
    for campaign in campaigns:
        for subset in campaigns_info[campaign]:
            for sample_info in subset.samples:
                sample_names = get_sample_group_list(
                    sample_info, subset.name, subset.derivation, summary
                )

                report = load_report(subset.file)
                if not report:
                    continue

                (
                    events_total,
                    events_done,
                    done_list,
                    _,
                    failed_list,
                    broken_list,
                    not_found_list,
                    _,
                ) = process_report(report, sample_names)

                sample_info.percentage = (
                    int(100 * events_done / events_total) if events_total else 0
                )
                sample_info.failed = len(failed_list) > 0
                sample_info.broken = len(broken_list) > 0
                sample_info.missing = len(not_found_list) > 0

            subsets[subset.url] = subset

    summary.tasks = subsets

    with file.open('w') as output:
        dump(summary, output, indent=2, default=converter)
