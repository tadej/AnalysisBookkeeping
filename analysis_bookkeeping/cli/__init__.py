"""The Analysis Bookkeeping Command Line Interface."""
import click

from .. import __version__
from .plots import plots
from .samples import samples
from .summary import summary
from .tasks import tasks


@click.group(context_settings=dict(help_option_names=['-h', '--help']))
@click.version_option(version=__version__)
def bookkeeping() -> None:
    """Top-level CLI entrypoint."""


bookkeeping.add_command(samples)
bookkeeping.add_command(tasks)
bookkeeping.add_command(plots)
bookkeeping.add_command(summary)

__all__ = ['bookkeeping']
