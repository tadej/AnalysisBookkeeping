"""The samples CLI group."""
import click
import re
from typing import List

from ..config import APP_DATA_PATH, SAMPLE_DATABASE_PATH
from ..common.cache import check_and_write
from ..samples.generate import (
    generate_aod_list,
    generate_daod_list,
    generate_daod_list_data,
    generate_pileup_list,
    generate_xsec_info,
)
from ..samples import Sample, SampleDSID, SamplesInfo, SamplesList


@click.group(name='samples')
def cli() -> None:
    """Run the samples CLI group."""


@cli.command()
def samples() -> None:
    """Generate samples info."""
    print('### Generating samples info')
    print('')

    path_dsid = SAMPLE_DATABASE_PATH / 'DSID'
    path_out = APP_DATA_PATH / 'samples'

    print(f'Looking for samples in: {path_dsid}')
    print(f'Data output in: {path_out}')
    print('')

    files_dsid = [
        f for f in path_dsid.iterdir() if f.is_file() and 'Data' not in f.name
    ]

    for file in files_dsid:
        with file.open('r') as f:
            samples: List[SampleDSID] = []
            samples_info: SamplesInfo = {}
            samples_out: SamplesList = []

            for line in f:
                line_split = line.split('#')
                dsid_str: str = line_split[0].strip()
                if not dsid_str:
                    continue

                dsid = int(dsid_str)
                label = line_split[1].strip()

                samples.append(dsid)
                sample = Sample(dsid, label)
                sample.MC = True
                samples_info[dsid] = sample

            generate_xsec_info(samples_info, file.name)
            generate_aod_list(samples_info, file.name)
            generate_pileup_list(samples_info, file.name)
            generate_daod_list(samples_info, file.name)

            for s in samples:
                samples_out.append(samples_info[s])

            file_out = file.name.replace('txt', 'json')
            file_out = file_out.replace('+', '__')
            check_and_write(path_out / file_out, samples_out)

    # Data
    with open(path_dsid / 'Data.txt', 'r') as f:
        samples = []
        samples_info = {}
        samples_out = []

        for line in f:
            line_split = line.split('#')
            dsid_str = line_split[0].strip()
            if not dsid_str:
                continue

            search_year = re.search('data([0-9][0-9])_13TeV', dsid_str)
            search_period = re.search(
                'data[0-9][0-9]_13TeV.period([A-Za-z]+)', dsid_str
            )
            if not search_year or not search_period:
                raise ValueError('Invalid data sample')

            year = 2000 + int(search_year.group(1))
            period = search_period.group(1)

            samples.append(dsid_str)
            sample = Sample(dsid_str, f'{year} data, period {period}')
            sample.MC = False
            sample.year = year
            sample.period = period
            samples_info[dsid_str] = sample

        generate_daod_list_data(samples_info)

        for s in samples:
            samples_out.append(samples_info[s])

        check_and_write(path_out / 'Data.json', samples_out)
