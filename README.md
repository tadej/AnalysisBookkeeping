# Analysis Bookkeeping

A python powered analysis samples, tasks and plots bookkeeping web page.

## Copyright info

Copyright (C) 2021 Tadej Novak

This code may be used under the terms of the
GNU Affero General Public License version 3.0 as published by the
Free Software Foundation and appearing in the file [LICENSE.md](LICENSE.md).
