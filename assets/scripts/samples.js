const Samples = {
  init: function() {
    const body = $('body');
    body.find('.sample .link-xsec').on("click", Samples.clickXsec);
    body.find('.sample .link-aod').on("click", Samples.clickAod);
    body.find('.sample .link-daod').on("click", Samples.clickDaod);
    body.find('.sample .link-ntup-pileup').on("click", Samples.clickNtupPileup);
    body.find('.sample .link-hide').on("click", Samples.clickHideAll);
  },

  clickXsec: function(event) {
    Samples.clickShowGeneric(event, 'list-xsec');
  },

  clickAod: function(event) {
    Samples.clickShowGeneric(event, 'list-aod');
  },

  clickDaod: function(event) {
    Samples.clickShowGeneric(event, 'list-daod');
  },

  clickNtupPileup: function(event) {
    Samples.clickShowGeneric(event, 'list-ntup-pileup');
  },

  clickShowGeneric: function(event, block) {
    const origin = $(event.currentTarget).parents('.sample');

    const list = origin.find('.' + block);
    if (list.is(':visible')) {
      Samples.clickHideAll(event);
    } else {
      Samples.clickHideAll(event, block);
      list.velocity('slideDown', { duration: 300, display: 'flex' });
    }
  },

  clickHideAll: function(event, exception) {
    const origin = $(event.currentTarget).parents('.sample');
    const blocks = ['list-xsec', 'list-aod', 'list-daod', 'list-ntup-pileup'];

    for (let list of blocks) {
      if (!exception || exception != list) {
        const el = origin.find('.' + list);
        if (el.is(':visible')) {
          el.velocity('slideUp', { duration: 300 });
        }
      }
    }
  }
};

export default Samples;
