import renderMathInElement from 'katex/contrib/auto-render/auto-render'

import Navigation from './navigation.js';
import Samples from './samples.js';

$(function () {
  Navigation.init();
  Samples.init();

  renderMathInElement(document.body);
});
